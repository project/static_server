<?php

namespace Drupal\static_server\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Static Server Config Form.
 */
class StaticServerConfigForm extends ConfigFormBase {

    /**
     * The messenger.
     *
     * @var \Drupal\Core\Messenger\MessengerInterface
     */
    protected $messenger;

    /**
     * Constructs a ProgressSettingsForm object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The factory for configuration objects.
     * @param \Drupal\Core\Messenger\MessengerInterface $messenger
     *   The messenger service.
     */
    public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
      parent::__construct($config_factory);
      $this->messenger = $messenger;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('config.factory'),
        $container->get('messenger'),
      );
    }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['static_server.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_static_server_configuration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $default_css = $base_url;
    $default_js = $base_url;
    $default_files = $base_url;
    $default_ext = 'gif jpg jpeg png';
    $config = $this->config('static_server.settings');

    if (!empty($config->get('static_server_css_path'))) {
      $default_css = $config->get('static_server_css_path');
    }
    if (!empty($config->get('static_server_js_path'))) {
      $default_js = $config->get('static_server_js_path');
    }
    if (!empty($config->get('static_server_files_path'))) {
      $default_files = $config->get('static_server_files_path');
    }
    if (!empty($config->get('static_server_files_extension'))) {
      $default_ext = $config->get('static_server_files_extension');
    }

  	$form['static_server']['static_server_css_path'] = [
      '#title' => $this->t("CSS Files Path"),
      '#type' => 'textfield',
      '#default_value' => $default_css,
    ];

    $form['static_server']['static_server_js_path'] = [
      '#title' => $this->t("JS Files Path"),
      '#type' => 'textfield',
      '#default_value' => $default_js,
    ];

    $form['static_server']['static_server_files_path'] = [
      '#title' => $this->t("Files Path"),
      '#type' => 'textfield',
      '#default_value' => $default_files,
    ];

    $form['static_server']['static_server_files_extension'] = [
      '#title' => $this->t("Files Extension"),
      '#type' => 'textfield',
      '#default_value' => $default_ext,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $static_css = $form_state->getValue('static_server_css_path');
    $static_js = $form_state->getValue('static_server_js_path');
    $static_file = $form_state->getValue('static_server_files_path');
    $static_extensions = $form_state->getValue('static_server_files_extension');

    // Load configuration object and save values.
    $config = $this->config('static_server.settings');
    $config->set('static_server_css_path', $static_css)->save();
    $config->set('static_server_js_path', $static_js)->save();
    $config->set('static_server_files_path', $static_file)->save();
    $config->set('static_server_files_extension', $static_extensions)->save();
  	$this->messenger->addStatus($this->t('The settings have been saved'));
  }
}
